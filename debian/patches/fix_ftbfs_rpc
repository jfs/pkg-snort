Description: Fix detection of libtirpc for Debian-based systems
Author: Javier Fernández-Sanguino Peña <jfs@debian.org>
Origin: vendor
Last-Update: 2020-12-06

--- a/configure.in
+++ b/configure.in
@@ -1037,13 +1037,69 @@
             echo
             echo " ERROR! tirpc not found, get it by running "
             echo " yum install libtirpc-devel "
-            exit
+            exit 1
         fi
         LIBS="${LIBS} -ltirpc"
         extra_incl="-I/usr/include/tirpc"
     fi
 fi
 
+###########################################################
+# Debian and Ubuntu do not have inbuilt SunRPC support    #
+# in glibc 2.26 and later and is separately available     #
+# in the libtirpc-dev package.                            #
+# Make sure we've got the library and link it             #
+###########################################################
+
+if test -f /etc/os-release ; then
+    DISTRO_ID=$(cat /etc/os-release |grep ^ID | awk -F = '{print $2}')
+    DISTRO_LIKE=$(cat /etc/os-release |grep ^ID_LIKE | awk -F = '{print $2}')
+    # Only continue if /usr/include/rpc/rpc.h does not exist or it exist but it is not provided by libc6
+    if test -f /usr/include/rpc/rpc.h ||  dpkg -S /usr/include/rpc/rpc.h 2>&1 |grep ^libc6 >/dev/null; then
+            echo " Found RPC (provided by glibc, via libc6-dev package)"
+    else
+        if test $DISTRO_ID = "debian" || test $DISTRO_LIKE = "debian" ; then
+            TIRPC=""
+            AC_CHECK_LIB(tirpc,bindresvport,, TIRPC="no")
+            echo "$TIRPC"
+            if test "x$TIRPC" = "xno"; then
+                echo
+                echo " ERROR! tirpc not found, get it by running "
+                echo " apt install libtirpc-devel "
+                exit 1
+            fi
+            LIBS="${LIBS} -ltirpc"
+            extra_incl="-I/usr/include/tirpc"
+        fi
+    fi
+fi
+
+
+# Alternative code to providing switchable RPC implementation
+# from https://wiki.gentoo.org/wiki/Glibc_2.26_porting_notes/RPC_implementation
+#AC_ARG_WITH([libtirpc],
+#      [AS_HELP_STRING([--with-libtirpc], [Use libtirpc as RPC implementation (instead of sunrpc)])])
+#
+#AS_IF([test "x$with_libtirpc" = xyes],
+#      [PKG_CHECK_MODULES([TIRPC],
+#                         [libtirpc],
+#                         [RPC_CFLAGS=$TIRPC_CFLAGS; RPC_LIBS=$TIRPC_LIBS;],
+#                        [AC_MSG_ERROR([libtirpc requested, but library not found.])]
+#                       )],
+#      [AC_CHECK_HEADER(rpc/rpc.h,
+#                      [RPC_CFLAGS=""; RPC_LIBS="";],
+#                      [AC_MSG_ERROR([sunrpc requested, but headers are not present.])]
+#                     )]
+#)
+#
+
+#AC_SUBST(RPC_CFLAGS)
+#AC_SUBST(RPC_LIBS)
+#
+# or
+# CFLAGS="$CFLAGS $RPC_CFLAGS"
+# LIBS="${LIBS} $RPC_LIBS"
+
 Z_LIB=""
 AC_CHECK_HEADERS(zlib.h,, Z_LIB="no")
 if test "x$Z_LIB" = "xno"; then
